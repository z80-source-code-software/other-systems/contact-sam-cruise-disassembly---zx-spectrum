<!DOCTYPE html>
<html>
<head>
<title>Contact Sam Cruise: Routine at 28660</title>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="../style.css" />
</head>
<body class="Asm-c">
<table class="header">
<tr>
<td class="logo"><a href="../index.html"><img alt="Contact Sam Cruise" src="../images/logo.png" /></a></td>
<td class="page-header">Routines</td>
</tr>
</table>
<table class="asm-navigation">
<tr>
<td class="prev">
Prev: <a href="28656.html">28656</a>
</td>
<td class="up">Up: <a href="../maps/all.html#28660">Map</a></td>
<td class="next">
Next: <a href="28702.html">28702</a>
</td>
</tr>
</table>
<div class="description">28660: Update the telephone icon</div>
<table class="disassembly">
<tr>
<td class="routine-comment" colspan="5">
<div class="details">
<div class="paragraph">
Used by the routine at <a href="29733.html">29733</a>. The main entry point is used to make the telephone appear to vibrate as if ringing.
</div>
</div>
</td>
</tr>
<tr>
<td class="address-2"><span id="28660"></span>28660</td>
<td class="instruction">LD HL,28722</td>
<td class="comment-1" rowspan="1">Point <span class="register">HL</span> at the graphic data for the telephone when ringing (at <a href="28722.html">28722</a>)</td>
</tr>
<tr>
<td class="address-1"><span id="28663"></span>28663</td>
<td class="instruction">NOP</td>
<td class="comment-1" rowspan="1"></td>
</tr>
<tr>
<td class="address-1"><span id="28664"></span>28664</td>
<td class="instruction">JR 28669</td>
<td class="comment-1" rowspan="1"></td>
</tr>
<tr>
<td class="routine-comment" colspan="5">
<span id="28666"></span>
<div class="comments">
<div class="paragraph">
This entry point is also used by the routine at <a href="29733.html">29733</a> to make the telephone appear still (as between rings, or when not ringing).
</div>
</div>
</td>
</tr>
<tr>
<td class="address-2"><span id="28666"></span>28666</td>
<td class="instruction">LD HL,28741</td>
<td class="comment-1" rowspan="1">Point <span class="register">HL</span> at the graphic data for the telephone when not ringing (at <a href="28741.html">28741</a>)</td>
</tr>
<tr>
<td class="address-2"><span id="28669"></span>28669</td>
<td class="instruction">LD DE,20654</td>
<td class="comment-1" rowspan="1">Point <span class="register">DE</span> at the display file address of the top pixel row of the top-left tile of the telephone icon</td>
</tr>
<tr>
<td class="address-1"><span id="28672"></span>28672</td>
<td class="instruction">LD A,6</td>
<td class="comment-1" rowspan="8">Update the top six pixel rows of the top two tiles of the telephone icon</td>
</tr>
<tr>
<td class="address-2"><span id="28674"></span>28674</td>
<td class="instruction">LDI</td>
</tr>
<tr>
<td class="address-1"><span id="28676"></span>28676</td>
<td class="instruction">LDI</td>
</tr>
<tr>
<td class="address-1"><span id="28678"></span>28678</td>
<td class="instruction">DEC E</td>
</tr>
<tr>
<td class="address-1"><span id="28679"></span>28679</td>
<td class="instruction">DEC E</td>
</tr>
<tr>
<td class="address-1"><span id="28680"></span>28680</td>
<td class="instruction">INC D</td>
</tr>
<tr>
<td class="address-1"><span id="28681"></span>28681</td>
<td class="instruction">DEC A</td>
</tr>
<tr>
<td class="address-1"><span id="28682"></span>28682</td>
<td class="instruction">JR NZ,28674</td>
</tr>
<tr>
<td class="address-1"><span id="28684"></span>28684</td>
<td class="instruction">LD A,(HL)</td>
<td class="comment-1" rowspan="6">Update the bottom two pixel rows of the top-left tile of the telephone icon</td>
</tr>
<tr>
<td class="address-1"><span id="28685"></span>28685</td>
<td class="instruction">LD (DE),A</td>
</tr>
<tr>
<td class="address-1"><span id="28686"></span>28686</td>
<td class="instruction">INC L</td>
</tr>
<tr>
<td class="address-1"><span id="28687"></span>28687</td>
<td class="instruction">INC D</td>
</tr>
<tr>
<td class="address-1"><span id="28688"></span>28688</td>
<td class="instruction">LD A,(HL)</td>
</tr>
<tr>
<td class="address-1"><span id="28689"></span>28689</td>
<td class="instruction">LD (DE),A</td>
</tr>
<tr>
<td class="address-1"><span id="28690"></span>28690</td>
<td class="instruction">LD B,3</td>
<td class="comment-1" rowspan="1">There are three more pixel rows to update</td>
</tr>
<tr>
<td class="address-1"><span id="28692"></span>28692</td>
<td class="instruction">LD DE,20686</td>
<td class="comment-1" rowspan="1">Point <span class="register">DE</span> at the display file address of the top pixel row of the bottom-left tile of the telephone icon</td>
</tr>
<tr>
<td class="address-2"><span id="28695"></span>28695</td>
<td class="instruction">INC L</td>
<td class="comment-1" rowspan="5">Update the top three pixel rows of the bottom-left tile of the telephone icon</td>
</tr>
<tr>
<td class="address-1"><span id="28696"></span>28696</td>
<td class="instruction">LD A,(HL)</td>
</tr>
<tr>
<td class="address-1"><span id="28697"></span>28697</td>
<td class="instruction">LD (DE),A</td>
</tr>
<tr>
<td class="address-1"><span id="28698"></span>28698</td>
<td class="instruction">INC D</td>
</tr>
<tr>
<td class="address-1"><span id="28699"></span>28699</td>
<td class="instruction">DJNZ 28695</td>
</tr>
<tr>
<td class="address-1"><span id="28701"></span>28701</td>
<td class="instruction">RET</td>
<td class="comment-1" rowspan="1"></td>
</tr>
</table>
<table class="asm-navigation">
<tr>
<td class="prev">
Prev: <a href="28656.html">28656</a>
</td>
<td class="up">Up: <a href="../maps/all.html#28660">Map</a></td>
<td class="next">
Next: <a href="28702.html">28702</a>
</td>
</tr>
</table>
<footer>
<div class="release">The complete Contact Sam Cruise RAM disassembly 20191116</div>
<div class="copyright">&#169; 1986 Microsphere Computer Services Ltd. &#169; 2019 Richard Dymond.</div>
<div class="created">Created using <a href="https://skoolkit.ca">SkoolKit</a> 8.0.</div>
<div><a href="../hex/asm/6FF4.html">Switch to hexadecimal</a>.</div>
</footer>
</body>
</html>